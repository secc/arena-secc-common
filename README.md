Arena SECC Common Libraries
===============================================================================
This project contains common libraries for SECC Arena functionality.

Setup
-----
In order to setup this project in Arena, follow these steps:
* Build the project
* Drop the resulting Arena.Custom.SECC.Common.Data.dll and Arena.Custom.SECC.Common.Util.dll files into your Arena bin directory

Development
-----
The Arena.Custom.SECC.Common.Data assembly contains basic data utilities that are commonly
used at SECC.  If a project is big enough to require more than a single database table
we prefer that the project have it's own data package.  The classes contained in this assempbly
are typically just extensions of the Arena DataLayer, single LINQ class, or POCO classes.

The Arena.Custom.SECC.Common.Utils assembly is useful for various types of common functionality.
We typically use this to house functionality that we tend to share with multiple projects or
reuse a lot.

Building a NuGet Package
-----
In a command prompt, cd to the root of the project (the directory with the .sln file) and execute:
> .nuget\NuGet.exe pack Util\Arena.Custom.SECC.Common.Util.csproj -IncludeReferencedProjects

[Southeast Christian Church](http://www.southeastchristian.org/)
-----
Southeast Christian Church in Louisville, Kentucky is an evangelical Christian church. In our 
mission to connect people to Jesus and one another, Southeast Christian Church has 
grown into a unified multisite community located in the Greater Louisville/Southern Indiana region. 
We have multiple campuses serving the specific needs of the areas in which they are located, 
while receiving centralized leadership and teaching from the campus on Blankenbaker Parkway.