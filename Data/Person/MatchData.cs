﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arena.Custom.SECC.Common.Data.Person
{
    public class MatchData
    {
        public Int32 PersonID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string PhoneType { get; set; }

        public string Campus { get; set; }

        public string StreetAddress { get; set; }

        public string ZipCode { get; set; }
    }
}
