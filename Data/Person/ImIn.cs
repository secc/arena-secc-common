﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Arena.Custom.SECC.Common.Data.Person
{
    public class ImIn: MatchData
    {
        public Boolean IsMember { get; set; }

        public Boolean AgreesWithSoF { get; set; }
    }
}
