﻿using Arena.Core;
using Arena.DataLayer.Organization;
using Arena.SmallGroup;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Arena.Custom.SECC.Data.SmallGroup
{
    public class GroupCollection : Arena.SmallGroup.GroupCollection
    {
        public new void LoadByLeaderPersonID(int leaderPersonID)
        {
            OrganizationData orgData = new OrganizationData();

            ArrayList arrayList = new ArrayList();
            arrayList.Add(new SqlParameter("@OrganizationId", (object)ArenaContext.Current.Organization.OrganizationID));
            arrayList.Add(new SqlParameter("@LeaderPersonID", (object)leaderPersonID));
            SqlDataReader groupByLeaderID = orgData.ExecuteReader("cust_secc_smgp_sp_get_groupByLeaderPersonID", arrayList);

            while (groupByLeaderID.Read())
            {
                base.Add(new Group(groupByLeaderID));
            }
            groupByLeaderID.Close();
        }
    }
}
