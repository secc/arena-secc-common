﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Arena.Custom.SECC.Common.Data.Auth.DataLayer;
using Arena.Core;

namespace Arena.Custom.SECC.Common.Data.Auth
{
    public class Device
    {
        #region Fields
        Core.Person mCreatedBy = null;
        Core.Person mModifiedBy = null;
        Core.Person mPerson = null;
        #endregion

        #region Properties
        public int AuthDeviceId { get; set; }
        public Guid DeviceKey { get; set; }
        public string DeviceId { get; set; }
        public int PersonId { get; set; }
        public String LoginId { get; set; }
        public string DeviceName { get; set; }
        public DateTime LastLogin { get; set; }
        public string CreatedByUserId { get; private set; }
        public string ModifiedByUserId { get; private set; }
        public DateTime DateCreated { get; private set; }
        public DateTime DateModified { get; private set; }
        public bool Active { get; set; }

        public Core.Person CreatedBy
        {
            get
            {
                if ( mCreatedBy == null )
                {
                    mCreatedBy = new Core.Person(CreatedByUserId);
                }

                return mCreatedBy;
            }
        }

        public Core.Person ModifiedBy
        {
            get
            {
                if ( mModifiedBy == null )
                {
                    mModifiedBy = new Core.Person(ModifiedByUserId);
                }

                return mModifiedBy;
            }
        }

        public Core.Person Person
        {
            get
            {
                if (mPerson == null)
                {
                    mPerson = new Core.Person(PersonId);
                }

                return mPerson;
            }
        }

        #endregion

        #region Constructor
        public Device()
        {
            Init();
        }

        public Device(int id)
        {
            Load( id );
        }

        public Device(String deviceId)
        {
            Load(deviceId);
        }

        public Device(Guid guid)
        {
            Load(guid);
        }

        public Device(DeviceData data)
        {
            Load(data);
        }

        #endregion

        #region Public

        public static List<Device> LoadDevices()
        {
            using ( AuthDataContext context = AuthContextHelper.GetContext() )
            {
                List<Device> Devices = context.DeviceDatas.Select( s => new Device( s ) ).ToList();
                return Devices;
            }
        }

        public static List<Device> LoadDevicesByPersonID(int PersonID)
        {
            using (AuthDataContext context = AuthContextHelper.GetContext())
            {

                List<Device> Devices = context.DeviceDatas.Where(q => q.person_id == PersonID).Select(s => new Device(s)).ToList();
                return Devices;
            }
        }

        public void Delete()
        {
            using (AuthDataContext context = AuthContextHelper.GetContext())
            {  
                DeviceData device = context.DeviceDatas.FirstOrDefault(s => s.auth_device_id == AuthDeviceId);
                if (device.auth_device_id > 0) 
                {
                    context.DeviceDatas.DeleteOnSubmit(device);
                }
                context.SubmitChanges();
                Init();
            }
        }

        public bool Save( string userId )
        {
            try
            {
                using ( AuthDataContext context = AuthContextHelper.GetContext() )
                {
                    DeviceData data;

                    if ( AuthDeviceId > 0 )
                    {
                        data = context.DeviceDatas.FirstOrDefault(s => s.auth_device_id == AuthDeviceId);

                        if ( data == null )
                        {
                            throw new ArgumentException( "Auth Device Id is not valid.", "DeviceId" );
                        }
                    }
                    else
                    {
                        if ( DeviceKeyInUse() )
                        {
                            throw new ArgumentException( "Device Key is already in use.", "DeviceKey" );
                        }

                        data = new DeviceData();
                        data.created_by = userId;
                        data.date_created = DateTime.Now;
                    }

                    data.device_id = DeviceId;
                    data.device_key = DeviceKey;
                    data.device_name = DeviceName;
                    data.person_id = PersonId;
                    data.login_id = LoginId;
                    data.last_login = LastLogin;
                    data.modified_by = userId;
                    data.date_modified = DateTime.Now;
                    data.active = Active;

                    if(AuthDeviceId <= 0)
                    {
                        context.DeviceDatas.InsertOnSubmit( data );
                    }

                    context.SubmitChanges();

                    if ( data.auth_device_id > 0 )
                    {
                        Load( data );
                        return true;
                    }

                    return false;
                }
            }
            catch ( Exception ex )
            {
                throw new Exception( "An error occurred while saving Device", ex );
            }
        }

        #endregion

        #region Private

        private void Init()
        {
            AuthDeviceId = 0;
            DeviceId = null;
            DeviceKey = Guid.NewGuid();
            DeviceName = null;
            LoginId = null;
            PersonId = 0;
            LastLogin = DateTime.Now;
            CreatedByUserId = null;
            ModifiedByUserId = null;
            DateCreated = DateTime.MinValue;
            DateModified = DateTime.MinValue;
            Active = true;

            mCreatedBy = null;
            mModifiedBy = null;
        }

        private void Load(int id)
        {
            using (AuthDataContext context = AuthContextHelper.GetContext())
            {
                var data = context.DeviceDatas.FirstOrDefault(s => s.auth_device_id == id);

                Load(data);
            }
        }

        private void Load(String deviceId)
        {
            using (AuthDataContext context = AuthContextHelper.GetContext())
            {
                var data = context.DeviceDatas.FirstOrDefault(s => s.device_id == deviceId);
                Load(data);
            }
        }

        private void Load( Guid key )
        {
            using ( AuthDataContext context = AuthContextHelper.GetContext() )
            {
                var data = context.DeviceDatas.FirstOrDefault(s => s.device_key == key);
                Load( data );
            }
        }

        private void Load( DeviceData data )
        {
            Init();

            if ( data != null )
            {
                AuthDeviceId = data.auth_device_id;
                DeviceId = data.device_id;
                DeviceKey = data.device_key;
                DeviceName = data.device_name;
                LoginId = data.login_id;
                PersonId = data.person_id;
                LastLogin = Convert.ToDateTime(data.last_login);
                CreatedByUserId = data.created_by;
                ModifiedByUserId = data.modified_by;
                DateCreated = data.date_created;
                DateModified = data.date_modified;
                Active = data.active;
            }
        }

        private bool DeviceKeyInUse()
        {
            Device s = new Device(DeviceKey);

            return s.AuthDeviceId > 0;
        }

        #endregion
    }
}
