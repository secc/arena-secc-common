﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arena.Custom.SECC.Common.Data.Auth.DataLayer
{
    class AuthContextHelper
    {
        public static AuthDataContext GetContext()
        {
            Arena.DataLib.SqlDbConnection conn = new DataLib.SqlDbConnection();
            return new AuthDataContext( conn.GetDbConnection() );
        }
    }
}