﻿using Arena.Core;
using Arena.Core.Communications;
using Arena.DataLayer.Organization;
using Arena.Organization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Arena.Custom.SECC.Common.Util
{
    public class TagManager
    {
        // Default to WebProspect
        Int32 memberStatus = 962;
        String ignoreEmail = null;
        String ignorePhone = null;

        DateTime ignoreDOB = new DateTime(1900, 1, 1);
        Regex ignoreAddress = new Regex(@"^N/?A$", RegexOptions.IgnoreCase);
        Regex ignoreZip = new Regex(@"^0*$", RegexOptions.IgnoreCase);


        // Values to process
        public Int32 TagId { get; set; }
        public Int32 EmailConfirmation { get; set; }
        public String UserId {get; set;}
        private String Campus { get; set; }
        private String StreetAddress { get; set; }
        private String ZipCode { get; set; }
        private String EmailOverride { get; set; }
        private String FirstNameOverride { get; set; }
        private String PhoneOverride { get; set; }
        private String PhoneType { get; set; }
        private Boolean SendEmail { get; set; }

        public TagManager(Int32 tagId)
        {
            TagId = tagId;
        }

        /// <summary>
        /// Add a person via person match to an arena tag.  
        /// </summary>
        /// <param name="matchData">The person match data</param>
        /// <param name="settings">Profile Custom Member Field dictionary (Key = Param Id, Value = The String Value)</param>
        /// <returns>The person id we've added.</returns>
        public Int32 AddToTag(Arena.Custom.SECC.Common.Data.Person.MatchData matchData, Dictionary<Int32, String> settings = null)
        {
            // Ignore anything that is slated for ignore data
            if (!String.IsNullOrEmpty(matchData.Email))
            {
                if (ignoreEmail != null && ignoreEmail.Contains(matchData.Email))
                {
                    matchData.Email = null;
                }
            }
            if (new Regex(@"[^\d]").Replace(matchData.PhoneNumber, "") == ignorePhone)
            {
                matchData.PhoneNumber = null;
                matchData.PhoneType = null;
            }
            if (matchData.StreetAddress != null && ignoreAddress.IsMatch(matchData.StreetAddress.Trim()))
            {
                matchData.StreetAddress = null;
            }
            if (matchData.ZipCode != null && ignoreAddress.IsMatch(matchData.ZipCode.Trim()))
            {
                matchData.ZipCode = null;
            }
            if (matchData.DateOfBirth == ignoreDOB)
            {
                // This will already be ignored
            }


            // Get all the phone number types
            LookupCollection lc = new LookupCollection();
            lc.LoadByType(38);
            Lookup phoneTypeObj = lc.Where(e => e.Value == matchData.PhoneType).FirstOrDefault();

            // Create an empty list of persons
            Person person = null;
            if (matchData.PersonID > 0)
            {
                // If we have a person id, just set the person
                person = new Person(matchData.PersonID);
            }
            else { 
                // Otherwise everything should be validated and we can do a person match
                PersonMatch pm = new PersonMatch();
                pm.FirstName = matchData.FirstName.Trim();
                pm.LastName = matchData.LastName.Trim();
                pm.BirthDate = matchData.DateOfBirth;
                if (!String.IsNullOrEmpty(matchData.PhoneNumber))
                {
                    pm.Phone = matchData.PhoneNumber.Trim();
                }
                if (!String.IsNullOrEmpty(matchData.Email))
                {
                    pm.Email = matchData.Email.Trim(); 
                }
                if (phoneTypeObj != null)
                {
                    pm.PhoneType = phoneTypeObj.Guid;
                }
                pm.MemberStatus = memberStatus;

                // Make sure every record is active
                pm.RecordStatus = Arena.Enums.RecordStatus.Active;

                person = pm.MatchOrCreateArenaPerson();
            }
            // We matched one person
            if (person != null)
            {
                PersonMatch.syncEmailPhone(matchData.Email, matchData.PhoneNumber, phoneTypeObj, person);

                Campus = matchData.Campus;
                StreetAddress = matchData.StreetAddress;
                ZipCode = matchData.ZipCode;
                EmailOverride = matchData.Email;
                FirstNameOverride = matchData.FirstName;
                PhoneOverride = matchData.PhoneNumber;
                PhoneType = matchData.PhoneType;
                SendEmail = true;
                return AddToTag(person, settings);
            }
            else
            {
                throw new Exception("Error: Unable to process request.");
            }

            return 0;
        }

        /// <summary>
        /// Add a person to a tag and set the custom member params included in the settings Dictionary
        /// </summary>
        /// <param name="matchData">The person match data</param>
        /// <param name="settings">Profile Custom Member Field dictionary (Key = Param Id, Value = The String Value)</param>
        /// <returns>The person id we've added.</returns>
        public Int32 AddToTag(Person person, Dictionary<Int32, String> settings = null)
        {
            String profileMemberNote = "";

            // If we have a nickname in Arena that varies from their first name
            // we'll use that, otherwise just use the name they entered on the web.
            String nickName = person.NickName;
            if (string.IsNullOrEmpty(nickName) || nickName == person.FirstName)
            {
                nickName = FirstNameOverride;
            }

            // Update the person's campus if applicable
            if (!(person.Campus.Name != null && 
                person.Campus.Name.ToLower().Contains(Campus.ToLower())))
            {
                // If this is an actual campus, go ahead and set it
                CampusCollection cc = new CampusCollection(1);
                Campus campusObj = cc.Where(c => c.Name.ToLower().Contains(Campus.ToLower())).FirstOrDefault();
                if (campusObj != null)
                {
                    person.Campus = campusObj;
                    person.Save(1, UserId, true);
                }
                else
                {
                    profileMemberNote += "Other Campus Selected: " + Campus + "\n";
                }
            }

            // Set the address if applicable
            if (!String.IsNullOrEmpty(StreetAddress)
                && !String.IsNullOrEmpty(ZipCode))
            {
                Address address = new Address(StreetAddress, "", "", "", ZipCode);
                Boolean addressSaved = false;
                if (address.Standardize()) 
                {
                    if (person.PrimaryAddress == null)
                    {
                        PersonAddress pa = new PersonAddress()
                        {
                            Address = address,
                            AddressType = new Lookup(SystemLookup.AddressType_Home),
                            Primary = true
                        };
                        person.Addresses.Add(pa);
                        person.SaveAddresses(1, UserId);
                        addressSaved = true;
                    }
                }
                if (!addressSaved)
                {
                    if (person.PrimaryAddress != null && person.PrimaryAddress.StreetLine1 != address.StreetLine1) {
                        profileMemberNote += "New Address Entered: " + address.ToString() + "\n";
                    }
                }
            }

            // Push them into the tag
            ProfileMember pm = new ProfileMember();
            pm.PersonID = person.PersonID;
            pm.ProfileID = TagId;
            pm.MemberNotes = profileMemberNote;
            pm.Save(UserId);

            foreach(KeyValuePair<Int32, String> kvp in settings) {
                ProfileMemberFieldValue pmfv = new ProfileMemberFieldValue(TagId, person.PersonID, kvp.Key);
                pmfv.SelectedValue = kvp.Value;
                pmfv.ProfileId = TagId;
                pmfv.PersonId = person.PersonID;
                pmfv.CustomFieldId = kvp.Key;
                pmfv.Save(UserId);
            }

            if (EmailConfirmation > 0)
            {
                // Fetch the email template and populate it
                AdvancedHTML email = new AdvancedHTML();
                email.TemplateID = EmailConfirmation;

                Dictionary<string, string> fields = new Dictionary<string, string>();
                fields.Add("##FirstName##", person.FirstName);
                fields.Add("##LastName##", person.LastName);
                fields.Add("##NickName##", nickName);

                if (!String.IsNullOrEmpty(EmailOverride))
                {
                    fields.Add("##Email##", EmailOverride);
                }
                else
                {
                    fields.Add("##Email##", person.Emails.FirstActive ?? "N/A");
                }

                if (PhoneOverride != null && PhoneType != null)
                {
                    fields.Add("##Phone##", PhoneOverride + " (" + PhoneType + ")");
                } 
                else if (person.Phones.FirstOrDefault() != null) 
                {
                    fields.Add("##Phone##", person.Phones.FirstOrDefault().Number + " (" + person.Phones.FirstOrDefault().PhoneType + ")");
                }
                else
                {
                    fields.Add("##Phone##", "N/A");
                }

                // See if they entered an "Other Campus"
                if (person.Campus != null && person.Campus.Name != Campus)
                {
                    fields.Add("##Campus##", "Other - " + Campus);
                }
                else if (person.Campus != null)
                {
                    fields.Add("##Campus##", person.Campus.Name);
                }
                else
                {
                    fields.Add("##Campus##", "N/A");
                }

                fields.Add("##PersonGuid##", person.Guid.ToString());

                fields.Add("##Notes##", pm.MemberNotes);

                // If we don't have any "To:" people configured in the email template, then
                // This email should be sent to the person who is being added to the tag (a confirmation).
                if (email.Template.ToPersons.Count == 0) {
                    email.Send(person.Emails.FirstActive, fields);
                }
                else
                {
                    String toString = String.Join("; ", email.Template.ToPersons.Select(p => p.Emails.FirstActive).ToList());
                    email.Send(toString, fields);
                }

            }
            return person.PersonID;
        }


    }
}
