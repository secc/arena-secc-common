﻿using Arena.Core;
using Arena.Core.Communications;
using Arena.DataLayer.Organization;
using Arena.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Arena.Custom.SECC.Common.Util
{
    public class ImIn
    {
        /* Dev Settings */
        Int32 connectingPastorFollowupTagId = 0;
        Int32 unmatchedRecordsTagId = 0;
        Int32 imInEmailConfirmation = 0;
        Int32 imInEmailProcessing = 0;
        Int32 imInFollowupEmail = 0;
        Int32 imInNextStepBlankenbaker = 0;
        Int32 imInNextStepOldham = 0;
        Int32 imInNextStepIndiana = 0;
        Int32 imInNextStepSouthwest = 0;
        Int32 imInNextStepOther = 0;
        String userId = null;
        Int32 memberStatus = 0;
        Int32 profileCampusCustomFieldID = 0;
        Int32 memberIndicationCustomFieldId = 0;
        String ignoreEmail = null;
        String ignorePhone = null;

        /* Prod Settings 
        Int32 connectingPastorFollowupTagId = 32653;
        Int32 unmatchedRecordsTagId = 32652;
        Int32 imInEmailConfirmation = 2815;
        Int32 imInEmailProcessing = 2824;
        Int32 imInFollowupEmail = 2816;
        Int32 imInNextStepBlankenbaker = 2835;
        Int32 imInNextStepOldham = 2836;
        Int32 imInNextStepIndiana = 2837;
        Int32 imInNextStepSouthwest = 2838;
        Int32 imInNextStepOther = 2839;
        String userId = "ImIn";
        Int32 memberStatus = 11821;
        Int32 profileCampusCustomFieldID = 18554;
        Int32 memberIndicationCustomFieldId = 18632;
        */

        DateTime ignoreDOB = new DateTime(1900, 1, 1);
        Regex ignoreAddress = new Regex(@"^N/?A$", RegexOptions.IgnoreCase);
        Regex ignoreZip = new Regex(@"^0*$", RegexOptions.IgnoreCase);

        // Attribute GUIDS
        Guid memberAttr = new Guid("B5BD75B0-9FAF-4E28-A956-DD5FD108244C");
        Guid formerMemberAttr = new Guid("B448B8E0-F8BE-46C1-AF08-A747294C033B");
        Guid unmatchedAttr = new Guid("5B3399B9-3D3B-423D-9C5F-87C4335968B6");
        Guid statementOfFaithAgreementAttr = new Guid("68D99900-591D-4D1A-A384-7E387D805AC1");
        Guid otherCampusNoteAttr = new Guid("5720252E-1D04-4995-B6BF-0F26A05364A1");
        Guid otherCampusSelectedAttr = new Guid("DD98ED61-D144-498F-A7E5-037F562E45B1");
        Guid confirmationEmailSentAttr = new Guid("4F2FA7C1-268D-43DF-A6A2-FC25DB959624");
        Guid membershipRecommitmentAttr = new Guid("F2BA512B-F213-443D-9908-2172A28C9743");
        Guid streetAddressAttr = new Guid("B47D0891-6376-40ED-B9A1-543FBECE682D");
        Guid zipCodeAttr = new Guid("A0963CC0-E7B2-4CEA-81EC-D05BE68AA942");

        // Values to process
        public DateTime? AgreesWithSoF {get; set;}
        public DateTime? MemberIndicator { get; set; }
        public String Campus { get; set; }
        public String StreetAddress { get; set; }
        public String ZipCode { get; set; }
        public String EmailOverride { get; set; }
        public String FirstNameOverride { get; set; }
        public String PhoneOverride { get; set; }
        public String PhoneType { get; set; }
        public Boolean SendEmail { get; set; }

        public ImIn()
        {
            OrganizationSettingCollection settings = new OrganizationSettingCollection(ArenaContext.Current.Organization.OrganizationID);
            try
            {
                connectingPastorFollowupTagId = Int32.Parse(settings.FindByKey("ImInConnectingPastorFollowupTagId").Value);
                unmatchedRecordsTagId = Int32.Parse(settings.FindByKey("ImInUnmatchedRecordsTagId").Value);
                imInEmailConfirmation = Int32.Parse(settings.FindByKey("ImInEmailConfirmation").Value);
                imInEmailProcessing = Int32.Parse(settings.FindByKey("ImInEmailProcessing").Value);
                imInFollowupEmail = Int32.Parse(settings.FindByKey("ImInFollowupEmail").Value);
                imInNextStepBlankenbaker = Int32.Parse(settings.FindByKey("ImInNextStepBlankenbaker").Value);
                imInNextStepOldham = Int32.Parse(settings.FindByKey("ImInNextStepOldham").Value);
                imInNextStepIndiana = Int32.Parse(settings.FindByKey("ImInNextStepIndiana").Value);
                imInNextStepSouthwest = Int32.Parse(settings.FindByKey("ImInNextStepSouthwest").Value);
                imInNextStepOther = Int32.Parse(settings.FindByKey("ImInNextStepOther").Value);
                userId = settings.FindByKey("ImInUserId").Value;
                memberStatus = Int32.Parse(settings.FindByKey("ImInMemberStatus").Value);
                profileCampusCustomFieldID = Int32.Parse(settings.FindByKey("ImInProfileCampusCustomFieldID").Value);
                memberIndicationCustomFieldId = Int32.Parse(settings.FindByKey("ImInMemberIndicationCustomFieldId").Value);
                ignoreEmail = settings.FindByKey("ImInIgnoreEmail").Value;
                ignorePhone = settings.FindByKey("ImInIgnorePhone").Value;
            }
            catch (Exception)
            {
                throw new Exceptions.ArenaApplicationException("Unable to load I'm In organization settings.  Please review all the settings.");
            }
        }

        public Boolean process(Arena.Custom.SECC.Common.Data.Person.ImIn imIn)
        {
            // Ignore anything that is slated for ignore data
            if (!String.IsNullOrEmpty(imIn.Email))
            {
                if (ignoreEmail.Contains(imIn.Email))
                {
                    imIn.Email = null;
                }
            }
            if (new Regex(@"[^\d]").Replace(imIn.PhoneNumber, "") == ignorePhone)
            {
                imIn.PhoneNumber = null;
                imIn.PhoneType = null;
            }
            if (imIn.StreetAddress != null && ignoreAddress.IsMatch(imIn.StreetAddress.Trim()))
            {
                imIn.StreetAddress = null;
            }
            if (imIn.ZipCode != null && ignoreAddress.IsMatch(imIn.ZipCode.Trim()))
            {
                imIn.ZipCode = null;
            }
            if (imIn.DateOfBirth == ignoreDOB)
            {
                // This will already be ignored
            }


            // Get all the phone number types
            LookupCollection lc = new LookupCollection();
            lc.LoadByType(38);
            Lookup phoneTypeObj = lc.Where(e => e.Value == imIn.PhoneType).FirstOrDefault();

            // Create an empty list of persons
            Person person = null;
            if (imIn.PersonID > 0)
            {
                // If we have a person id, just set the person
                person = new Person(imIn.PersonID);
            }
            else { 
                // Otherwise everything should be validated and we can do a person match
                PersonMatch pm = new PersonMatch();
                pm.FirstName = imIn.FirstName.Trim();
                pm.LastName = imIn.LastName.Trim();
                pm.BirthDate = imIn.DateOfBirth;
                if (!String.IsNullOrEmpty(imIn.PhoneNumber))
                {
                    pm.Phone = imIn.PhoneNumber.Trim();
                }
                if (!String.IsNullOrEmpty(imIn.Email)) {
                    pm.Email = imIn.Email.Trim(); 
                }
                if (phoneTypeObj != null)
                {
                    pm.PhoneType = phoneTypeObj.Guid;
                }
                pm.MemberStatus = memberStatus;

                // Make sure every record is active
                pm.RecordStatus = Arena.Enums.RecordStatus.Active;

                person = pm.MatchOrCreateArenaPerson();
            }
            // We matched one person
            if (person != null)
            {
                PersonMatch.syncEmailPhone(imIn.Email, imIn.PhoneNumber, phoneTypeObj, person);
                DateTime? sofAgreeDate= null;
                if(imIn.AgreesWithSoF) sofAgreeDate = DateTime.Now;
                DateTime? memberDate = null;
                if (imIn.IsMember) memberDate = DateTime.Now;

                // Set the class vars and process the person
                AgreesWithSoF = sofAgreeDate;
                MemberIndicator = memberDate;
                Campus = imIn.Campus;
                StreetAddress = imIn.StreetAddress;
                ZipCode = imIn.ZipCode;
                EmailOverride = imIn.Email;
                FirstNameOverride = imIn.FirstName;
                PhoneOverride = imIn.PhoneNumber;
                PhoneType = imIn.PhoneType;
                SendEmail = true;
                processPerson(person);
            }
            else
            {
                throw new Exception("Error: Unable to process I'm In request.");
            }

            return true;
        }

        /// <summary>
        /// Process a person for I'm In
        /// </summary>
        /// <param name="person">The person who has responded to I'm In</param>
        /// <param name="agreesWithSoF">Indication that they agreed with the Statement of Faith</param>
        /// <param name="memberIndicator">Indication that they thought they think they are already a member</param>
        /// <param name="campus">The campus this person indicated</param>
        /// <param name="streetAddress">The person's new/updated street address</param>
        /// <param name="zipCode">The person's new/updated zip code</param>
        /// <param name="emailOverride">Specify the exact address to send the email to</param>
        /// <returns></returns>
        public Boolean processPerson(Person person)
        {
            // If we have a nickname in Arena that varies from their first name
            // we'll use that, otherwise just use the name they entered on the web.
            String nickName = person.NickName;
            if (string.IsNullOrEmpty(nickName) || nickName == person.FirstName)
            {
                nickName = FirstNameOverride;
            }

            // Update the person's campus if applicable
            if (!(person.Campus.Name != null && 
                person.Campus.Name.ToLower().Contains(Campus.ToLower())))
            {
                // If this is an actual campus, go ahead and set it
                CampusCollection cc = new CampusCollection(1);
                Campus campusObj = cc.Where(c => c.Name.ToLower().Contains(Campus.ToLower())).FirstOrDefault();
                if (campusObj != null)
                {
                    person.Campus = campusObj;
                    person.Save(1, userId, true);
                }
                else
                {
                    // If it doesn't match, set it in the other field.
                    PersonAttribute attribute = new PersonAttribute(person.PersonID, new Core.Attribute(otherCampusSelectedAttr).AttributeId);
                    attribute.IntValue = 1;
                    attribute.Save(1, userId);

                    attribute = new PersonAttribute(person.PersonID, new Core.Attribute(otherCampusNoteAttr).AttributeId);
                    attribute.StringValue = Campus;
                    attribute.Save(1, userId);
                }
            }

            // If they agree with the statement of faith, set the date.
            if (AgreesWithSoF.HasValue)
            {
                PersonAttribute attribute = new PersonAttribute(person.PersonID, new Core.Attribute(statementOfFaithAgreementAttr).AttributeId);
                attribute.DateValue = AgreesWithSoF.Value;
                attribute.Save(1, userId);
            }

            // Set the address if applicable
            if (person.MemberStatus.Guid == unmatchedAttr
                && !String.IsNullOrEmpty(StreetAddress)
                && !String.IsNullOrEmpty(ZipCode))
            {
                PersonAddress address = new PersonAddress()
                {
                    Address = new Address(StreetAddress, "", "", "", ZipCode),
                    AddressType = new Lookup(SystemLookup.AddressType_Home),
                    Primary = true
                };
                person.Addresses.Add(address);
                person.SaveAddresses(1, userId);
            }
            else
            {
                if ((!String.IsNullOrEmpty(StreetAddress) && person.PrimaryAddress != null && !(person.PrimaryAddress.StreetLine1.Contains(StreetAddress) || StreetAddress.Contains(person.PrimaryAddress.StreetLine1)))
                    || (!String.IsNullOrEmpty(ZipCode) && person.PrimaryAddress != null && !person.PrimaryAddress.PostalCode.Contains(ZipCode)))
                {
                    PersonAttribute attribute = new PersonAttribute(person.PersonID, new Core.Attribute(streetAddressAttr).AttributeId);
                    attribute.StringValue = StreetAddress;
                    attribute.Save(1, userId);

                    attribute = new PersonAttribute(person.PersonID, new Core.Attribute(zipCodeAttr).AttributeId);
                    attribute.StringValue = ZipCode;
                    attribute.Save(1, userId);
                }
            }

            Boolean sendNextStepEmail = false;

            // Load the former member attribute
            PersonAttribute formerMember = new PersonAttribute(person.PersonID, new Core.Attribute(formerMemberAttr).AttributeId);


            // They are a member or a former.  Woot!
            if ((person.MemberStatus.Guid == memberAttr 
                || formerMember.HasDateValue)
                && AgreesWithSoF.HasValue 
                && AgreesWithSoF.Value > new DateTime(1900, 1, 1))
            {
                PersonAttribute attribute = new PersonAttribute(person.PersonID, new Core.Attribute(membershipRecommitmentAttr).AttributeId);
                attribute.DateValue = MemberIndicator.HasValue?MemberIndicator.Value:DateTime.Now;
                attribute.Save(1, userId);

                // If the former member value is set, we need to update the person's member status too
                if (formerMember.HasDateValue)
                {
                    person.MemberStatus = new Lookup(memberAttr);
                    person.Save(1, userId, false);
                }

                // Fetch the email template and populate it
                AdvancedHTML email = new AdvancedHTML();
                email.TemplateID = imInEmailConfirmation;

                Dictionary<string, string> fields = new Dictionary<string, string>();
                fields.Add("##FirstName##", person.FirstName);
                fields.Add("##LastName##", person.LastName);
                fields.Add("##NickName##", nickName);

                // Send the email
                if (String.IsNullOrEmpty(EmailOverride))
                {
                    email.Send(person.Emails.FirstActive, fields);
                }
                else
                {
                    email.Send(EmailOverride, fields);
                }
                SendEmail = false;
                
                attribute = new PersonAttribute(person.PersonID, new Core.Attribute(confirmationEmailSentAttr).AttributeId);
                attribute.DateValue = DateTime.Now;
                attribute.Save(1, userId);
            }
            else if (person.MemberStatus.Guid == unmatchedAttr)
            { 
                // This is an unmatched record.  Unwoot.)
                ProfileMember pm = new ProfileMember();
                pm.PersonID = person.PersonID;
                pm.ProfileID = unmatchedRecordsTagId;
                pm.Save(userId);

                // Save their member indicator
                ProfileMemberFieldValue pmfv = new ProfileMemberFieldValue(connectingPastorFollowupTagId, person.PersonID, memberIndicationCustomFieldId);
                pmfv.SelectedValue = MemberIndicator.HasValue.ToString();
                pmfv.ProfileId = unmatchedRecordsTagId;
                pmfv.PersonId = person.PersonID;
                pmfv.CustomFieldId = memberIndicationCustomFieldId;
                pmfv.Save(userId);
                
            }
            else
            {
                // They are not a member so just push them into the
                // Connecting Pastor Followup tag
                ProfileMember pm = new ProfileMember();
                pm.PersonID = person.PersonID;
                pm.ProfileID = connectingPastorFollowupTagId;

                if (person.MemberStatus.Guid == memberAttr
                    && !AgreesWithSoF.HasValue
                    || (AgreesWithSoF.HasValue && AgreesWithSoF.Value == new DateTime(1900, 1, 1)))
                {
                    pm.MemberNotes += "Already is a member but did not agree to Statement of Faith.";
                }
                else if (formerMember.HasDateValue 
                    && !AgreesWithSoF.HasValue
                    || (AgreesWithSoF.HasValue && AgreesWithSoF.Value == new DateTime(1900, 1, 1)))
                {
                    pm.MemberNotes += "Is a former member but did not agree to Statement of Faith.";

                }
                else if (MemberIndicator.HasValue)
                {
                    pm.MemberNotes += "Believes they are already a member.";
                }
                else
                {
                    pm.MemberNotes += "Would like to become a member.";
                    if (!String.IsNullOrEmpty(EmailOverride)
                        || person.Emails.Active.Count > 0)
                    { 
                        sendNextStepEmail = true;
                        // Set the status to inactive because the "Next Step Email" takes care of the first
                        // communication with this person
                        pm.Status = new Lookup(SystemLookup.TagMemberStatus_Inactive);
                    }
                    else
                    {
                        pm.MemberNotes += "\nNo Email Address.";
                    }
                }


                pm.Save(userId);

                ProfileMemberFieldValue pmfv = new ProfileMemberFieldValue(connectingPastorFollowupTagId, person.PersonID, profileCampusCustomFieldID);
                pmfv.SelectedValue = Campus;
                pmfv.ProfileId = connectingPastorFollowupTagId;
                pmfv.PersonId = person.PersonID;
                pmfv.CustomFieldId = profileCampusCustomFieldID;
                pmfv.Save(userId);


                // Fetch the email template and populate it
                AdvancedHTML email = new AdvancedHTML();
                email.TemplateID = imInFollowupEmail;

                Dictionary<string, string> fields = new Dictionary<string, string>();
                fields.Add("##FirstName##", person.FirstName);
                fields.Add("##LastName##", person.LastName);
                fields.Add("##NickName##", nickName);

                if (!String.IsNullOrEmpty(EmailOverride))
                {
                    fields.Add("##Email##", EmailOverride);
                }
                else
                {
                    fields.Add("##Email##", person.Emails.FirstActive ?? "N/A");
                }

                if (PhoneOverride != null && PhoneType != null)
                {
                    fields.Add("##Phone##", PhoneOverride + " (" + PhoneType + ")");
                } 
                else if (person.Phones.FirstOrDefault() != null) 
                {
                    fields.Add("##Phone##", person.Phones.FirstOrDefault().Number + " (" + person.Phones.FirstOrDefault().PhoneType + ")");
                }
                else
                {
                    fields.Add("##Phone##", "N/A");
                }

                // See if they entered an "Other Campus" note:
                PersonAttribute campusAttribute = new PersonAttribute(person.PersonID, new Core.Attribute(otherCampusNoteAttr).AttributeId);
                if (campusAttribute.HasStringValue)
                {
                    fields.Add("##Campus##", "Other - " + campusAttribute.StringValue);
                }
                else if (person.Campus != null)
                {
                    fields.Add("##Campus##", person.Campus.Name);
                }
                else
                {
                    fields.Add("##Campus##", "N/A");
                }

                fields.Add("##PersonGuid##", person.Guid.ToString());

                fields.Add("##Notes##", pm.MemberNotes);


                // Fetch the email template and populate it
                AdvancedHTML nextStepEmail = new AdvancedHTML();

                // Send the email
                switch(person.Campus.CampusId)
                {
                    case 2:
                        if (!sendNextStepEmail) { 
                            email.Template.ReplyEmail = "imin.indiana@secc.org";
                            email.Send("imin.indiana@secc.org", fields);
                        }
                        nextStepEmail.TemplateID = imInNextStepIndiana;
                        break;
                    case 5:
                        if (!sendNextStepEmail)
                        {
                            email.Template.ReplyEmail = "imin.oldham@secc.org";
                            email.Send("imin.oldham@secc.org", fields);
                        }

                        nextStepEmail.TemplateID = imInNextStepOldham;
                        break;
                    case 7:
                        if (!sendNextStepEmail) { 
                            email.Template.ReplyEmail = "imin.southwest@secc.org";
                            email.Send("imin.southwest@secc.org", fields);
                        }

                        nextStepEmail.TemplateID = imInNextStepSouthwest;
                        break;
                    default:
                        if (!sendNextStepEmail) { 
                            email.Template.ReplyEmail = "imin.blankenbaker@secc.org";
                            email.Send("imin.blankenbaker@secc.org", fields);
                        }

                        if (campusAttribute.HasStringValue)
                        {
                            nextStepEmail.TemplateID = imInNextStepOther;
                        }
                        else { 
                            nextStepEmail.TemplateID = imInNextStepBlankenbaker;
                        }
                        break;
                }

                if (sendNextStepEmail)
                {
                    // Send the email
                    if (String.IsNullOrEmpty(EmailOverride))
                    {
                        nextStepEmail.Send(person.Emails.FirstActive, fields);
                    }
                    else
                    {
                        nextStepEmail.Send(EmailOverride, fields);
                    }

                }

            }

            // If the email should be sent, send it!
            if (SendEmail && !sendNextStepEmail)
            {
                // Fetch the email template and populate it
                AdvancedHTML email = new AdvancedHTML();
                email.TemplateID = imInEmailProcessing;

                Dictionary<string, string> fields = new Dictionary<string, string>();
                fields.Add("##FirstName##", person.FirstName);
                fields.Add("##LastName##", person.LastName);
                fields.Add("##NickName##", nickName);

                // Send the email
                if (String.IsNullOrEmpty(EmailOverride))
                {
                    email.Send(person.Emails.FirstActive, fields);
                }
                else
                {
                    email.Send(EmailOverride, fields);
                }

            }
            return true;
        }

    }
}
