﻿using Arena.Core;
using Arena.DataLayer.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Arena.Custom.SECC.Common.Util
{
    /**
     * This is a class used for person matching.  Some of the logic contained here was
     * reused from the Voracity Onetime giving module but it has been heavily modified
     */
    public class PersonMatch
    {
        Person CurrentPerson = Arena.Core.ArenaContext.Current.Person;
        Organization.Organization CurrentOrganization = Arena.Core.ArenaContext.Current.Organization;
        GenericPrincipal CurrentUser = Arena.Core.ArenaContext.Current.User;

        public String FirstName {get; set;}
        public String LastName { get; set; }
        private String _email = null;
        public String Email { 
            get { 
                // Always make sure we return a lowercase version of the email address
                return String.IsNullOrEmpty(_email) ? "" : _email.ToLower(); 
            } 
            set {
                _email = value;
            }
        }
        public String HomePhone { get; set; }
        public String Phone { get; set; }
        public Guid PhoneType { get; set; }
        public DateTime? BirthDate { get; set; }
        public Int32 MemberStatus { get; set; }
        public Int32 FamilyId { get; set; }
        public Enums.RecordStatus RecordStatus { get; set; }
        public Int32 Campus { get; set; }
        public PersonMatchAddress Address = new PersonMatchAddress();
        public String CurrentModule { get; set;}
        private List<List<String>> _diminutiveNames;
        public List<List<String>> DiminutiveNames {get {
            if (_diminutiveNames == null)
            {
                _diminutiveNames = new List<List<String>>();
                var assembly = Assembly.GetExecutingAssembly();
                string[] contents = {};

                using (Stream stream = assembly.GetManifestResourceStream(typeof(ImIn).Namespace + ".names.csv"))
                using (StreamReader reader = new StreamReader(stream))
                {
                    contents = reader.ReadToEnd().Replace("\r", "").Split('\n');
                }
                _diminutiveNames.AddRange(from line in contents
                          select (line.Split(',')).ToList());
            }
            return _diminutiveNames;
        }}
        private static Lookup AltPhoneType = new Lookup(new Guid("1D568B33-60C5-46A1-AA45-845318B0CDFB"));

        /// <summary>
        /// Default Constructor
        /// </summary>
        public PersonMatch()
        {
            // This is defaulted to Web Prospect for SECC
            MemberStatus = 962;

            // Default the record status to Pending
            RecordStatus = Arena.Enums.RecordStatus.Pending;
            
            // Default to BlankenBaker for SECC
            Campus = 1;

            // Default to online giving
            CurrentModule = "Voracity.OnlineGiving.OneTime";
        }
        /// <summary>
        /// This method uses the class properties to get a person if they
        /// are an exact arena match otherwise it creates a new person
        /// </summary>
        /// <returns>Arena Person</returns>
        public Person MatchOrCreateArenaPerson()
        {

            Person person = null;

            String firstNameLower = FirstName.Trim().ToLower();
            String lastNameLower = LastName.Trim().ToLower();

            if (CurrentPerson != null &&
                CurrentPerson.LastName.Trim().ToLower() == lastNameLower &&
                (CurrentPerson.FirstName.Trim().ToLower() == firstNameLower ||
                CurrentPerson.NickName.Trim().ToLower() == firstNameLower))
            {
                person = CurrentPerson;
            }
            else
            {
                List<Person> matches = GetMatches();

                if (matches.Count == 1)
                    person = matches[0];
            }

            if (person == null)
            {
                person = CreatePerson();
            }
            UpdateAndSavePerson(person);

            return person;
        }

        /// <summary>
        /// Create a person from the PersonMatch properties in this object.
        /// </summary>
        /// <returns>The newly created person object.</returns>
        public Person CreatePerson()
        {

            Person person = new Person();
            person.FirstName = FirstName.Trim();
            person.LastName = LastName.Trim();
            if (BirthDate.HasValue
                && BirthDate != new DateTime(1900, 1, 1))
            {
                person.BirthDate = BirthDate.Value;
            }
            person.RecordStatus = RecordStatus;


            try
            {
                person.MemberStatus = new Lookup(MemberStatus);
                if (person.MemberStatus.LookupID == -1)
                    throw new Exception("Member Status setting must be a valid Member Status Lookup value.");
            }
            catch (System.Exception ex)
            {
                throw new Exception("Member Status setting must be a valid Member Status Lookup value.", ex);
            }

            if (Campus != 0)
                try { person.Campus = new Arena.Organization.Campus(Campus); }
                catch { person.Campus = null; }

            person.MaritalStatus = new Lookup(SystemLookup.MaritalStatus_Unknown);
            person.Gender = Arena.Enums.Gender.Unknown;
            return person;
        }

        /// <summary>
        /// Update and save the person (If there are any changes needed).
        /// </summary>
        /// <param name="person">The person to save.</param>
        public void UpdateAndSavePerson(Person person)
        {
            Boolean newPerson = person.PersonID <= 0;
            Boolean savePerson = false;
            Lookup HomePhoneType = new Lookup(SystemLookup.PhoneType_Home);
            PersonPhone PersonHomePhone = person.Phones.FindByType(HomePhoneType.LookupID);
            if (PersonHomePhone == null && HomePhone != null && HomePhone.Trim() != string.Empty)
            {
                PersonHomePhone = new PersonPhone();
                PersonHomePhone.PhoneType = HomePhoneType;
                person.Phones.Add(PersonHomePhone);
                PersonHomePhone.Number = HomePhone.Trim();
                savePerson = true;
            }

            if (Phone != null && PhoneType != null)
            {
                Lookup thePhoneType = new Lookup(PhoneType);
                PersonPhone thePhone = person.Phones.FindByType(HomePhoneType.LookupID);
                if (thePhone == null && Phone != null && Phone.Trim() != string.Empty)
                {
                    thePhone = new PersonPhone();
                    thePhone.PhoneType = thePhoneType;
                    person.Phones.Add(thePhone);
                    thePhone.Number = Phone.Trim();
                    savePerson = true;
                }
            }

            Lookup HomeAddressType = new Lookup(SystemLookup.AddressType_Home);
            PersonAddress HomeAddress = person.Addresses.FindByType(HomeAddressType.LookupID);
            // Make sure we at least attempt to standardize the address
            Address.Standardize();
            if (HomeAddress == null && !String.IsNullOrEmpty(Address.Street))
            {
                HomeAddress = new PersonAddress();
                HomeAddress.AddressType = HomeAddressType;
                HomeAddress.Primary = true;

                foreach (PersonAddress prevPersonAddress in person.Addresses)
                    prevPersonAddress.Primary = false;

                person.Addresses.Add(HomeAddress);

                HomeAddress.Address = new Address(
                    Address.Street.Trim(),
                    Address.StreetLine2!=null?Address.StreetLine2.Trim():String.Empty,
                    Address.City.Trim(),
                    Address.State.Trim(),
                    Address.PostalCode.Trim(),
                    true);
                savePerson = true;
            }

            string currentUserName = CurrentModule;
            if (CurrentUser != null && CurrentUser.Identity != null && !string.IsNullOrEmpty(CurrentUser.Identity.Name))
                currentUserName = CurrentUser.Identity.Name;

            // For existing people, we only need to save if we've received new address/phone numbers
            // when we didn't have that data before
            if (savePerson)
            {
                person.Save(CurrentOrganization.OrganizationID, currentUserName, false);
                person.SaveAddresses(CurrentOrganization.OrganizationID, currentUserName);
                person.SavePhones(CurrentOrganization.OrganizationID, currentUserName);
            }

            if (newPerson)
            {
                // The only time we allow updating emails if it's a new person
                if (Email != null && Email.Trim() != string.Empty)
                    person.Emails.FirstActive = Email.Trim();
                person.SaveEmails(CurrentOrganization.OrganizationID, currentUserName);

                if (FamilyId <= 0)
                {
                    Family family = new Family();
                    family.OrganizationID = CurrentOrganization.OrganizationID;
                    family.FamilyName = person.LastName + " Family";
                    family.Save(currentUserName);
                    FamilyId = family.FamilyID;
                }

                FamilyMember fm = new FamilyMember(FamilyId, person.PersonID);
                fm.FamilyID = FamilyId;
                fm.FamilyRole = new Lookup((fm.Age < 18)?SystemLookup.FamilyRole_Child:SystemLookup.FamilyRole_Adult);
                fm.Save(currentUserName);
            }
        }

        public List<Person> GetMatches() {
            // Just stop if we don't have a last name
            if (String.IsNullOrEmpty(LastName))
            {
                return new List<Person>();
            }

            // Make sure to attempt to standardize the address first
            if (Address.Street != null)
            {
                Address.Standardize();
            }

            // First find all matching people
            DataTable personTable = new PersonData().GetPersonList_DS(
                Address.Street??"", 
                null, -1, 
                BirthDate??DateTime.Parse("1900-01-01"), 
                Email??"", 
                -1, "", FirstName, LastName, 
                -1,  (HomePhone!=null?HomePhone:(Phone!=null?Phone:"")), 
                -1, 
                true, 0, 
                0, 0, -1, 
                true, -1, -1, 
                -1, null,
                DateTime.Parse("1900-01-01"), -1, 
                string.Empty, 
                Arena.Core.ArenaContext.Current.Organization.OrganizationID);


            var results = from personRow in personTable.AsEnumerable() select personRow;
            List<Person> persons = results.Where(s => s.Field<Int32>("member_status") != 962)
                .Select(s => new Person(s.Field<Int32>("person_id"))).ToList();

            // Only include people in the family if the familyid is set
            if (FamilyId > 0)
            {
                persons = persons.Where(p => p.FamilyId == FamilyId).ToList();
            }
            // If we didn't get any results, relax the search a little to see if we can find
            // a diminutive first name and/or the 
            // email or phonenumber elsewhere in the household
            if (persons.Count == 0)
            {
                personTable = new PersonData().GetPersonList_DS(
                    Address.Street ?? "",
                    null, -1,
                    BirthDate ?? DateTime.Parse("1900-01-01"),
                    "",
                    -1, "", "", LastName,
                    -1, "",
                    -1,
                    true, 0,
                    0, 0, -1,
                    true, -1, -1,
                    -1, null,
                    DateTime.Parse("1900-01-01"), -1,
                    string.Empty,
                    Arena.Core.ArenaContext.Current.Organization.OrganizationID);

                // Iterate through all the people matching on last name and birthdate
                foreach (DataRow dr in personTable.Rows)
                {
                    // Skip existing web prospects
                    if (dr.Field<Int32>("member_status") == 962)
                    {
                        continue;
                    }

                    String lastName = dr.Field<String>("last_name").ToLower();
                    Boolean firstNameMatch = String.IsNullOrEmpty(FirstName);
                    if (lastName.Equals(LastName.ToLower()) && !firstNameMatch) {
                        String firstNameLower = String.IsNullOrEmpty(dr.Field<String>("first_name")) ? "" : dr.Field<String>("first_name").ToLower();
                        String nickNameLower = String.IsNullOrEmpty(dr.Field<String>("nick_name")) ? "" : dr.Field<String>("nick_name").ToLower();
                        firstNameMatch = firstNameLower.Equals(FirstName.ToLower()) || nickNameLower.Equals(FirstName.ToLower());

                        // If the first name isn't already matched (exact)
                        if (!firstNameMatch)
                        {
                            foreach (List<String> Names in DiminutiveNames)
                            {
                                if (Names.Contains(FirstName.ToLower()) &&
                                    (Names.Contains(firstNameLower) ||
                                    Names.Contains(nickNameLower)))
                                {
                                    firstNameMatch = true;
                                }
                            }
                        }
                    }
                    // If we've matched the first name, go ahead and carry on
                    if (lastName.Equals(LastName.ToLower()) && firstNameMatch)
                    {
                        Person person = new Person(dr.Field<Int32>("person_id"));
                        Boolean phoneMatch = !String.IsNullOrEmpty(HomePhone) ? isPhoneInFamily(HomePhone, person) : !String.IsNullOrEmpty(Phone) ? isPhoneInFamily(Phone, person) : false;
                        Boolean emailMatch = !String.IsNullOrEmpty(Email) && isEmailInFamily(Email, person);
                        
                        // Stop as soon as we find a match (or if the person doesn't have an email or phone number)
                        if ((person.Phones.Count == 0 && person.Emails.Count == 0)
                            || phoneMatch || emailMatch)
                        {
                            // This person matched so push them into the list
                            persons.Add(person);
                        }
                    }
                }
            }

            // Only include people in the family if the familyid is set
            if (FamilyId > 0)
            {
                persons = persons.Where(p => p.FamilyId == FamilyId).ToList();
            }

            return persons;
        }

        /// <summary>
        /// Sync the email/phone number with the person.  This is used for updating the email OR phone number
        /// for situations where we are matching using one or the other and need to update their record.
        /// </summary>
        /// <param name="email">The email address to check/sync.</param>
        /// <param name="phone">The phone to check/sync.</param>
        /// <param name="phoneType">The type of phone to save this as if necessary.</param>
        /// <param name="person">Person the person for which to save these records.</param>
        /// <returns></returns>
        public static Boolean syncEmailPhone(String email, String phone, Lookup phoneType, Person person)
        {
            try { 
                // Check to see if the email we are getting is updated and needs to be merged.
                if (!String.IsNullOrEmpty(email))
                {
                    // We only store this email if it doesn't exist on one of the other family members
                    if (!PersonMatch.isEmailInFamily(email, person)) {
                        PersonEmail sourceEmail = new PersonEmail();
                        sourceEmail.Email = email;
                        sourceEmail.PersonId = person.PersonID;

                        if (person.Emails.Count > 0 && person.Emails.FirstActive.ToLower().Contains("secc.org"))
                        {
                            // Do not set this as the primary email.
                            sourceEmail.Order = person.Emails.Count;
                        }
                        else
                        {
                            // Set the new one as the primary email.
                            sourceEmail.Order = 0;
                            foreach(PersonEmail pe in person.Emails){
                                pe.Order++;
                            }
                        }
                        person.Emails.Add(sourceEmail);
                        person.Emails.Save(sourceEmail.PersonId, 1, "SECC Person Match");                                        
                    }
                }

                // Now check to see if the phone number is updated and needs to be merged.
                if (!String.IsNullOrEmpty(phone))
                {
                    if (!PersonMatch.isPhoneInFamily(phone, person))
                    {
                        PersonPhone sourcePhone = new PersonPhone();
                        sourcePhone.Number = phone;
                        sourcePhone.PhoneType = phoneType;
                        sourcePhone.PersonID = person.PersonID;
                        List<PersonPhone> deletePhones = new List<PersonPhone>();
                        foreach (PersonPhone targetPhone in person.Phones)
                        {
                            if (targetPhone.PhoneType.Guid == sourcePhone.PhoneType.Guid)
                            {
                                // Set the old phone type to alternate
                                targetPhone.PhoneType = AltPhoneType;
                            }
                            else if (targetPhone.PhoneType.Guid == AltPhoneType.Guid)
                            {
                                deletePhones.Add(targetPhone);
                            }
                        }
                        // Remove any phones listed for delete
                        foreach (PersonPhone deletePhone in deletePhones)
                        {
                            person.Phones.Remove(deletePhone);
                        }
                        person.Phones.Add(sourcePhone);
                        person.SavePhones(1, "ImIn");
                    }
                }
            }
            catch (Exception)
            {
                // Don't throw anything--just return false;
                return false;
            }
            return true;
        }

        /// <summary>
        /// Check to see if a phone number exists in the family
        /// </summary>
        /// <param name="phone">The phone number to look for</param>
        /// <param name="person">The person who's family we want to check</param>
        /// <returns></returns>
        public static Boolean isPhoneInFamily(String phone, Person person)
        {
                foreach (FamilyMember fm in person.Family().FamilyMembers)
                {
                    foreach (PersonPhone p in fm.Phones)
                    {
                        Regex regexObj = new Regex(@"[^\d]");
                        // Only compare the digits
                        if (regexObj.Replace(p.Number, "") == regexObj.Replace(phone, ""))
                        {
                            return true;
                        }
                    }
                }
                return false;
        }


        /// <summary>
        /// Check to see if an eamil exists in the family
        /// </summary>
        /// <param name="phone">The email to look for</param>
        /// <param name="person">The person who's family we want to check</param>
        /// <returns></returns>
        public static Boolean isEmailInFamily(String email, Person person)
        {
            // Try to find an email match anywhere in the family
            foreach (FamilyMember fm in person.Family().FamilyMembers)
            {
                foreach (PersonEmail e in fm.Emails)
                {
                    if (e.Email.ToLower().Equals(email.ToLower()))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    }

    public class PersonMatchAddress {
        private Boolean _isStandardized = false;

        public String Street { get; set; }

        public String StreetLine2 { get; set; }

        public String City { get; set; }

        public String State { get; set; }

        public String PostalCode { get; set; }

        public bool Standardize() {
            // If we've already standardized this, just return
            if (_isStandardized)
            {
                return true;
            }

            Address address = new Address();
            if (Street != null) {
                address.StreetLine1 = Street;
            }
            if (StreetLine2 != null)
            {
                address.StreetLine2 = StreetLine2;
            }
            if (City != null) {
                address.City = City;
            }
            if (State != null) {
                address.State = State;
            }
            if (PostalCode != null)
            {
                address.PostalCode = PostalCode;
            }
            try { 
                if(address.Standardize())
                {
                    _isStandardized = true;
                    Street = address.StreetLine1;
                    StreetLine2 = address.StreetLine2;
                    City = address.City;
                    State = address.State;
                    PostalCode = address.PostalCode;
                }
            }
            catch
            {
                // Catch everything and just swallow the exception
            }
            return false;
        }
    }
}
